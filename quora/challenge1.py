import quorachallenge


the_string = input('Enter an alphanumeric string:')
my_segment_counter = 0
segments = []
prev_letter = ''
multiple_letters = ''
for letters in the_string:
    if letters != prev_letter:
        segments.append(letters)
        my_segment_counter += 1
    else:
        multiple_letters = segments[my_segment_counter -1]
        multiple_letters += letters
        segments[my_segment_counter -1] = multiple_letters
        prev_letter = letters
print(segments)
print(my_segment_counter)
