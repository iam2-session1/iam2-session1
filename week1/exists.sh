#!/bin/bash

for arg in $*; do

if [ ! $arg ]; then
    echo "argument expected" 
else
    if [ ! -f $arg ]; then
       echo "file does not exist"
    fi
fi
done
