#!/bin/bash

for arg in $*; do

if [ ! $arg ]; then
    echo "argument expected" 
else
    if [ ! -f $arg ]; then
       echo "$arg does not exist"
    else
       echo "inspected by $USER" >> $arg
    fi
fi
done

